package layouts.sourceit.com.airplanemode;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class BluetoothReceiver extends BroadcastReceiver{

    public static final int ID = 101;
    public static final int NOTIFY_ID = 500;

    @Override
    public void onReceive(Context context, Intent intent) {
        String bluetoothState = isBluetoothEnabled(context) ? "Big Brother is still watching you" : "Bluetooth Disabled";
        Log.d("bluetooth", "state");
        Toast.makeText(context, bluetoothState,Toast.LENGTH_LONG).show();

        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(context, ID, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager nm1 =  (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        Notification.Builder builder1 = new Notification.Builder(context);

        builder1.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("Bluetooth")
                .setLights(Color.BLUE, 500, 500)
                .setContentText(bluetoothState);
        Notification n1 = builder1.build();
        nm1.notify(NOTIFY_ID, n1);
    }

    private static boolean isBluetoothEnabled (Context context) {
        return Settings.Global.getInt(context.getContentResolver(), Settings.Global.BLUETOOTH_ON, 0) != 0;
    }
}
