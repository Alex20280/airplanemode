package layouts.sourceit.com.airplanemode;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.util.Log;

import static android.os.Build.ID;

public class AirplaneModeReceiver extends BroadcastReceiver {

    public static final int ID = 101;
    public static final int NOTIFY_ID = 500;

    @Override
    public void onReceive(Context context, Intent intent) {

        String airplaneModestate = isAirplaneModeEnabled(context) ? "Big Brother is watching you" : "AirplaneMode Disabled";
        Log.d("airmode", "state");

        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(context, ID, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager nm =  (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("AirplaneMode")
                .setLights(Color.BLUE, 500, 500)
                .setContentText(airplaneModestate);

        Notification n = builder.build();
        nm.notify(NOTIFY_ID, n);

    }
    private static boolean isAirplaneModeEnabled(Context context) {
        return Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }
}
