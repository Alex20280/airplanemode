package layouts.sourceit.com.airplanemode;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    TextView wifi;
    ToggleButton wifiButton;
    TextView status;
    TextView bluetooth;
    ToggleButton blButton;
    TextView blStatus;

    private Context mContext;
    private Resources mResources;
    private BluetoothAdapter mBluetoothAdapter;
    private static final int REQUEST_ENABLE_BT = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wifi = (TextView) findViewById(R.id.wifi);
        wifiButton = (ToggleButton) findViewById(R.id.wifiButton);
        status = (TextView) findViewById(R.id.status);

        bluetooth = (TextView) findViewById(R.id.bluetooth);
        blButton = (ToggleButton) findViewById(R.id.blButton);
        blStatus = (TextView) findViewById(R.id.blStatus);

        mContext = getApplicationContext();
        mResources = getResources();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        wifiButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    toggleWiFi(true);
                    status.setText("Wifi Enabled");
                } else {
                    toggleWiFi(false);
                    status.setText("Wifi Disabled");
                }
            }
        });


        blButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                    blStatus.setText("Bluetooth Enabled");
                } else {
                    try {
                        mBluetoothAdapter.disable();
                    } catch (Exception e) {
                        blStatus.setText("Bluetooth Disabled");
                    }
                }


            }
        });
    }

    public void toggleWiFi(boolean status) {
        WifiManager wifiManager = (WifiManager) this.getSystemService(WIFI_SERVICE);
        if (status && !wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        } else if (!status && wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
    }

}
